#include "shell.h"
#include "../stlio.h"
#include "../mem.h"
#include "../pit.h"

char line[LINE_LENGTH];

void jmain(void) {
	memset((u8*)line, 0x00, LINE_LENGTH);
	printf("Welcome to jankos\n"); 
	printf("Some basic commands to get you started: \n"
			"     timer: prints out current timer tick\n"
			"     quit: hangs because power management vry hrd\n");
	printf("Where's the prompt? \n     It literally causes triple faults so its invisible :^)\n"
			"_Also the cursor is busted and i don't wanna fix it\n");
	while(1) {
		memset((u8*)line, 0x00, LINE_LENGTH);
		read(line, LINE_LENGTH);
		if(strlen(line) == 1) {
			continue;
		}
		if(!strcmp("quit\n", line)) {
			printf("Well I guess we'll hang now\n");
			break;
		}
		if(!strcmp("timer\n", line)) {
			printf("Time tick: ");
			printhex(pit_timer_ticks); putch('\n');
		}
	}
}

void strip_newline(char* line) {
	for(u32 i = 0;i <strlen(line); i++) {
		if(line[i] == '\n') {
			line[i] = '\0';
		}
	}
}
