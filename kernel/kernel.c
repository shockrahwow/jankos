#include "kernel.h"
#include "types.h"
#include "interrupts.h"
#include "gdt.h"
#include "pit.h"
#include "kbd.h"
#include "stlio.h"
#include "mem.h"

#ifdef TESTING
	#include "tests.h"
#endif

#include "jank-shell/shell.h"

void kinit() {
	//gdt_configure();
	init_idt();
	pit_install_timer();
	kbd_install_keyboard();
#ifdef TESTING
	test_dispatcher();
#endif
}


// Should kmain return, we fall back to the loader which then just keeps in a hung state
void kmain() {
	kinit();
	clear_fb();
	jmain();
}
