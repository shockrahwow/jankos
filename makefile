CC=gcc
ASM=nasm
LINK=ld
ISO=genisoimage

BUILD_DIR=build

COMPONENTS=loader
# Rebuild the components var to be usable in recipes
OBJECTS := $(addprefix build/,$(addsuffix .o,$(COMPONENTS)))

AFLAGS=-f elf32
CFLAGS=-masm=intel -O2 -m32 -nostdlib -nostdinc -fno-builtin -fno-stack-protector -nostartfiles \
		-nodefaultlibs -Wall -Wextra -c #-Werror -c

compile: kernel.elf

# Link objects together to produce the kernel object
kernel.elf: $(COMPONENTS)
	$(LINK) -T link.ld -melf_i386 $(OBJECTS) -o build/$@

# Builds image of our os
jos.iso: kernel.elf
	cp kernel.elf iso/boot/kernel.elf
	$(ISO) -R							\
		-b boot/grub/stage2_eltorito    \
		-no-emul-boot                   \
		-boot-load-size 4               \
		-A os                           \
		-input-charset utf8             \
		-boot-info-table                \
		-o jos.iso                       \
		iso

# Components which make up the  OS
loader: loader/main.s
	$(ASM) $(AFLAGS) $< -o $(BUILD_DIR)/$@.o


# Running (no recipes called) 
run: jos.iso
	bochs -f bochsrc.conf -q 

clean:
	rm -f build/*
	rm -rf *.o kernel.elf jos.iso bochslog.txt
