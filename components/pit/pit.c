#include "pit.h"
#include "types.h"
#include "interrupts.h"
#include "serial.h"
#include "stlio.h"

void pit_inc_ticks(__attribute__((unused)) struct cpu_reg_state* cpu) {
    pit_timer_ticks++;
}

void pit_timer_wait(u32 time) {
	u32 ticks = time + pit_timer_ticks;
	while(pit_timer_ticks < ticks);
}

void pit_install_timer(void) {
    init_irq_handler(0, pit_inc_ticks); // timer interrupt request falls into irq 0
	pit_timer_ticks = 0;
}
