#ifndef PIT_H
#define PIT_H
#include "interrupts.h"
#include "types.h"

// 60hz timer for now
#define PIT_TIMER_INTERVAL 100
#define PIT_TIME_DATA0 0x40
#define PIT_TIME_DATA1 0x41
#define PIT_TIME_DATA2 0x42

volatile u32 pit_timer_ticks;

void pit_inc_ticks(struct cpu_reg_state*);

void pit_timer_wait(u32);

void pit_install_timer(void);
#endif
