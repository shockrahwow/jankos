#include "types.h"

#ifndef INCLUDE_IO_H
#define INCLUDE_IO_H

void serialport_write_byte(const u16, const u16);

u8 serial_read_byte(const u16);
#endif /* INCLUDE_IO_H */
