#include "gdt.h"
// Congirures a given gdt entry base on the entry index given
void gdt_configure_entry(u32 entry, u32 base, u32 limit, u8 access, u8 gran) {
	// base address
	gdt_entries[entry].low_base = (base & 0xffff);
	gdt_entries[entry].middle_base = (base >> 16) & 0xff;
	gdt_entries[entry].high_base = (base >> 24) & 0xff;

	// descriptor limits
	gdt_entries[entry].low_limit = (limit & 0xffff);
	gdt_entries[entry].granular = ((limit >> 16) & 0x0f);

	gdt_entries[entry].granular |= (gran & 0xf0);
	gdt_entries[entry].access = access;
}

// Configure the gdt pointer desribed in gdt.h as type GDT_PTR
void gdt_configure() {
	// gdt_ptr configuration
	gdt_ptr.size = (sizeof(struct GDT_Entry) * NO_GDT_ENTRIES) - 1;

	// First entry is just the null descriptor and is left along overall
	gdt_configure_entry(0, 0, 0, 0, 0);

	// Second entry: code segment base adders
	// Base address: 0 
	// limit: 4GB
	// Granularity: 4KB
	// 32-bit opcodes
	// Code segment selector
	gdt_configure_entry(1, 0, 0xffffffff, 0x9a, 0xcf); // code segment

	// Finally the data segment
	// All the same except the desriptor type specifies that its data
	gdt_configure_entry(2, 0, 0xffffffff, 0x92, 0xcf); // data segment

	// user mode segments
	gdt_configure_entry(3, 0, 0xffffffff, 0xfa, 0xcf);
	gdt_configure_entry(4, 0, 0xffffffff, 0xf2, 0xcf);

	// Load in the new changes to the gdt
	load_gdt();
}
