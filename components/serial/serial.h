#include "types.h"
#include "ports.h"

// Serial driver interface

#define SERIAL_COM1_BASE 				0x03F8

#define SERIAL_DATA_PORT(base) 			(base)
#define SERIAL_DATA_PORT_INT_EN(base)	(base+1)
#define SERIAL_FIFO_COMMAND_PORT(base) 	(base+2)
#define SERIAL_LINE_COMMAND_PORT(base) 	(base+3)
#define SERIAL_MODEM_COMMAND_PORT(base) (base+4)
#define SERIAL_LINE_STATUS_PORT(base) 	(base+5)

#define SERIAL_LINE_ENABLE 				0x80

// Default configurations for serial ports/lines/buffers etc.
// Rational can be found here: https://littleosbook.github.io/#configuring-the-buffers
#define SERIAL_DEFAULT_LINE_CFG 0x03
#define SERIAL_DEFAULT_BUFFER_CFG 0xc7
#define SERIAL_DEFAULT_MODEM_CFG 0x03

#define SERIAL_FIFO_EMPTY_CODE 0x20

#define PIC1_PORT 0x20
#define PIC2_PORT 0xA0

#define PIC1_START_INT 0x20
#define PIC1_END_INT 0x27
#define PIC2_START_INT 0x28
#define PIC2_END_INT 0x2F

#define PIC_ACK 0x20

void serial_set_baud_rate(const u16, const u16);

void serial_configure_line(const u16, const u8); 

u8 serial_fifo_empty(const u16);

u64 serial_write(const char* buffer, const u64 size);

void serial_pic_ack(u32 interrupt);
