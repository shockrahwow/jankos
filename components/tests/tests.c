// Module for testing drivers and other things
#include "stlio.h"
#include "tests.h"
#include "serial.h"
#include "shell.h"

void test_serial_write() {
	char* serial1 = "0123456789abcdef0123456789abcdef";
	serial_write(serial1, strlen(serial1));
	printf("serial test write finished\n");
}

void test_write(void) {
	char* msg1 = "Writing to fbout\n";
	printf(msg1);
}

void test_dispatcher(void) {
	clear_fb();
}
void test_read(void) {
	char buf[5];
	u32 bread = read(buf, 5);
	printf("Bytes read: "); printhex(bread);
	printf("\n");
	printf("String read: "); printf(buf);
	printf("\n");
	for(u32 i = 0;i<5;i++) {
		printhex(buf[i]); printf(" ");
	}
}
