#include "mem.h"
#include "types.h"

u32 memcpy(u8* src, u8* dest, const u32 size) {
	u32 i;
	for(i =0; i < size+1;i++) {
		dest[i] = src[i];
	}
	return i;
}

u32 memset(u8* buf, const u8 val, const u32 size) {
	u32 i;
	for(i=0;i<size;i++) {
		buf[i] = val;
	}
	return i;
}
