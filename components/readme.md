# Components Library

These are the smaller components that give the kernel actual behaviors.
These are pieces that are *practically* required for the OS to function at a 
basic level but otherwise are not *technically* required for sheer load-level
operations.