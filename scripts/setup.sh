#!/bin/bash
[ $(id -u) -ne 0 ] && echo Must run as root && exit 0

dpkg -s bochs
if [ $? -ne 0 ];then
	apt install bochs-x
fi

gen=`dpkg --list | grep genisoimage`
if [ -z "$gen" ]; then
	apt-get install -y genisoimage
fi

nasm=`dpkg --list | grep nasm`
if [ -z "$nasm" ]; then 
	apt-get install -y nasm
fi
