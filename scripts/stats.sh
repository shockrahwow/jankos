#!/bin/sh

# Some basic Metrics about the project itself
# NOTE: requires termgraph to be installed `pip3 install termgraph should do it for most unix systems`

c_code=`wc -l *.c *.h | tail -1 | awk '{print $1}'`
s_code=`wc -l *.s | tail -1 | awk '{print $1}'`
m_code=`wc -l makefile link.ld bochsrc.conf | tail -1 | awk '{print $1}'`
sh_code=`wc -l scripts/*.sh | tail -1 | awk '{print $1}'`

echo C $c_code > stats.dat
echo ASM $s_code >> stats.dat
echo Misc"(LinkScripts/Configs)" $m_code >> stats.dat
echo Shell $sh_code >> stats.dat

termgraph stats.dat --color blue --title "Lines of Code per language"
echo `wc -l *.c *.h *.s makefile link.ld bochsrc.conf scripts/*sh | tail -1` lines of code
echo Total commits: `git rev-list --all --count`

rm -f  stats.dat
