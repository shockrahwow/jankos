; Since we have no stack we have to create one for the OS

global loader

; NOTE: these symbols are what we use to represent the "magic bytes" for the whole os
MAGIC_NUMBER 	equ 0x1BADB002
FLAGS 			equ 0x0
CHECKSUM 		equ -MAGIC_NUMBER

; extern kmain

section .text
; align all instructions to 4 byte boundary by the x86 instruction set law
align 4
	; dropping our magic and other things into memory
	dd MAGIC_NUMBER
	dd FLAGS
	dd CHECKSUM

; sets up our gdt and segment selectors for later use
	
;loader:
;	call kmain

loader:
    .loop:
        mov eax, 0xfeedbeef
        jmp .loop
        mov eax, 0xfeedbeef


